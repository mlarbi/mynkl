import requests
from helpers.user_datas import build_interesting_datas, building_datas
import nltk
from nltk.metrics import distance
from nltk.util import ngrams
import numpy as np
from nltk.metrics import jaccard_distance


# fetch les ids des collections déposées par le user
def fetch_collections_id(datas: list):
    identifier_list: list = []
    for data in datas["data"]:
        if (data["identifier"].startswith('10.34847')):
            identifier_list.append(data["identifier"])
    return identifier_list


# fetch les données affiliées à une collection du user
def fetch_datas_by_ids(identifier: str, baseurl: str, api_key: str):

    try:
        response = requests.get(
            f"{baseurl}/collections/{identifier}/datas", headers={"x-api-key": api_key})
        return response.json()
    except Exception as error:
        print(error)


# construit un tableau de métadonnées pour chaque collections
def build_metadatas_by_collections(ids_list: list, baseUrl, api_key):
    list_metadatas_by_collections: list = []
    for collection in ids_list:
        result = fetch_datas_by_ids(collection, baseUrl, api_key)
        if (result["data"]):
            list_metadatas_by_collections.append(
                build_interesting_datas(result["data"]))
    return list_metadatas_by_collections


# créer un tableau regroupant toutes les métas interessantes des données, par collection
def fetch_metas_by_collection(datas: list, baseUrl: str, api_key: str):
    collections_ids_list = fetch_collections_id(datas)
    list_collections_meta = build_metadatas_by_collections(
        collections_ids_list, baseUrl, api_key)
    return list_collections_meta


def build_metadatas_from_current_donnee(data):
    interesting_donnee = building_datas(data)

    return interesting_donnee


def cosine_similarity(text1, text2):
    # Tokenize the texts
    tokens1 = nltk.word_tokenize(text1)
    tokens2 = nltk.word_tokenize(text2)
    print(tokens1)
    print(tokens2)

    # Calculate the bigrams for each text
    bigrams1 = ngrams(tokens1, 2)
    bigrams2 = ngrams(tokens2, 2)

    # Calculate the Jaccard similarity
    similarity = distance.jaccard_distance(set(bigrams1), set(bigrams2))

    return similarity


def jaccard_similarity(text1, text2):
    set1 = set(text1)
    set2 = set(text2)
    return 1 - jaccard_distance(set1, set2)


def calcul_similarity(data: str, nakala_data_clean: list, nakala_data: list):
    best_matches = []
    for i, text1 in enumerate([data]):
        similarities = []
        for text2 in nakala_data_clean:
            similarity = jaccard_similarity(text1.split(), text2.split())
            similarities.append(similarity)
        best_match_index = np.argmax(similarities)
        best_matches.append(best_match_index)
        print("Best match for text {} in texts1: text {} in texts2 with similarity {}".format(
            i, best_match_index, max(similarities)))

    print("Best matches:", best_matches)
    best_matches_filtered = list(set(best_matches))
    find_datas = [nakala_data[i] for i in best_matches_filtered]
    return find_datas


def send_final_recommandations(nakala_datas: list, data_user: str):

    clean_nakala_datas = []
    for data in nakala_datas:
        new_data = building_datas(data)
        clean_nakala_datas.append(new_data)

    # # permet de réduire une liste en une seule valeur en exécutant une fonction à chaque étape
    # clean_user_data= reduce(lambda acc, val: acc + ["".join(val)], datas_user, [])

    similarity_datas = calcul_similarity(
        data_user, clean_nakala_datas, nakala_datas)
    return similarity_datas
