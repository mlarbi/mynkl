from flask import Flask

from tf_idf import stemmed_info, tfidf


def build_interesting_datas(data):
    """build correct text for define a data with importants metas

    Args:
        data: data from users or from Nakala

    Returns:
        _type_: datas with only interestings metadatas
    """
   
    
    if data:
        meta_datas = ""
        if data["metas"]:
            for meta in data["metas"]:
                if meta["propertyUri"] == "http://nakala.fr/terms#created":
                    if meta["value"] != None:
                        meta_datas += str(meta["value"]) + ' '
                if meta["propertyUri"] == "http://purl.org/dc/terms/subject":
                    meta_datas += str(meta["value"]) + ' '
                if meta["propertyUri"] == "http://purl.org/dc/terms/description":
                    meta_datas += str(meta["value"]) + ' '
                if meta["propertyUri"] == "http://nakala.fr/terms#title":
                    meta_datas += str(meta["value"]) + ' '
    return meta_datas


def building_datas(data):
    created = next((meta['value'] for meta in data['metas'] if meta['propertyUri'] == 'http://nakala.fr/terms#created'), '')
    subjects = ', '.join(meta['value'] for meta in data['metas'] if meta['propertyUri'] == 'http://purl.org/dc/terms/subject')
    description = next((meta['value'] for meta in data['metas'] if meta['propertyUri'] == 'http://purl.org/dc/terms/description'), '')
    title = next((meta['value'] for meta in data['metas'] if meta['propertyUri'] == 'http://nakala.fr/terms#title'), '')
    
    return f"{created}\n{subjects}\n{description}\n{title}"
