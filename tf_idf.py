import nltk
import math
import re
import string
from nltk.corpus import stopwords
from nltk.stem.porter import PorterStemmer


text1: str = "Un atome. Un at(ome est composé d'un noyau! - lui-même (constitué) de protons et de neutrons - (entouré) d'un cortège d' électrons qui gravitent en orbite autour du noyau. Le noyau porte une charge positive - les protons sont chargés positivement, alors que les neutrons sont neutres électriquement comme leur nom l'indique."
text2: str = "la focaccia est la version italienne de la fougasse en fait, on la déguste soit nature comme du pain, soit aux olives, aux tomates séchées, aux oignons, aux herbes...Son goût est bluffant ! Vous allez vous régaler et épater tout le monde avec cette focaccia moelleuse, parfumée, délicieuse !"
text3: str = "François-Marie Arouet, dit Voltaire, né le 21 novembre 1694 à Paris où il meurt le 30 mai 1778, est un écrivain, philosophe, dramaturge, poète et encyclopédiste français qui a marqué le XVIIIᵉ siècle."
text4: str = "Résultat de recherche d'images pour 'noyau'Le noyau est un organite délimité qui participe à l'organisation de la cellule eucaryote. Le noyau sert de support à l'enveloppe nucléaire. En effet, les lamines (protéines organisées en maille perpendiculaire) forment une structure qui soutient la membrane nucléaire en périphérie du noyau."


def get_tokens(text):
    lowers = text.lower()
    chars_to_remove = "!#()«»’?:|,\.``" 
    rx = '[' + re.escape(chars_to_remove) + ']'
    no_parenthese = re.sub(rx, '', lowers)
    no_punctuation = no_parenthese.translate(string.punctuation)
    tokens = nltk.word_tokenize(no_punctuation)
 
    return tokens


def stem_tokens(tokens, stemmer):
    stemmed = []
    for item in tokens:
        stemmed.append(stemmer.stem(item))
    return stemmed


def stemmed_info(text):
    tokens = get_tokens(text)
    filtered = [w for w in tokens if w not in stopwords.words(
        ['english', 'french', 'spanish'])]

    stemmer = PorterStemmer()
    stemmed = stem_tokens(filtered, stemmer)
    
    return stemmed


def tf(word, document):
    return document.count(word) / (len(document) * 1.)


def n_containing(word, liste_document):
    return sum(1. for document in liste_document if word in document)


def idf(word, liste_document):
    return math.log(len(liste_document) / (1. + n_containing(word, liste_document)))


def tfidf(word, document, liste_document):
    return tf(word, document) * idf(word, liste_document)


# document1 = stemmed_info(text1)
# document2 = stemmed_info(text2)
# document3 = stemmed_info(text3)
# document4 = stemmed_info(text4)

# # define the quantity of the output key words
# N = 10
# documentlist = [document1, document2, document3, document4]
# print(documentlist)
# # define a list to save the key words
# documentkeywords = list()
# for i, document in enumerate(documentlist):
#     print("Top words in document {}".format(i + 1))
#     scores = {word: tfidf(word, document, documentlist) for word in document}
#     sorted_words = sorted(scores.items(), key=lambda x: x[1], reverse=True)
#     for word, score in sorted_words[:N]:
#         print("\tWord: {}, TF-IDF: {}".format(word, round(score, 5)))
#         words = list()
#     for i in range(N):
#         words.append(sorted_words[i])

#     documentkeywords.append(words)


# users_interests = documentkeywords
# # calcule the similarity and sort


# def most_similar_file(array):
#     similarity = list()
#     sim = 0.0
#     for i in range(len(documentlist)):
#         for j in range(len(array)):
#             for k in range(1, 10):

#                 if (array[j] == users_interests[i][k][0]):
#                     sim = sim + 1.0*users_interests[i][k][1]

#         similarity.append((i, sim))
#         sim = 0.0

#     sorted_sim = sorted(similarity,
#                         key=lambda x: x[1],
#                         reverse=True)
#     return sorted_sim


# # According to the type of the input element, use different steps
# type = "not wolrd"
# if (type == "mots"):
#     num_array = list()
#     doc = ""
#     num = ("Enter how many elements you want:")

#     for i in range(int(num)):
#         n = ("num :")
#         num_array.append(str(n))
#         doc = doc + str(n) + " "
#     print('ARRAY: ', num_array)
#     print('par ordre decroissant de similarite (numero_document, similarite)')
#     print(most_similar_file(stemmed_info(doc)))
# else:
#     path = "/home/lelia/Bureau/Simplon/Hashbang/mynkl-back2/data.txt"
#     file = open(path, 'r', encoding='utf-8')
#     text = file.read()

#     document = stemmed_info(text)

#     results = most_similar_file(document)
#     print(results)
#     for data in results:

#         print(data[0] + 1)
